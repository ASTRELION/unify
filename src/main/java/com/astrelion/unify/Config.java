package com.astrelion.unify;

import com.electronwill.nightconfig.core.CommentedConfig;
import com.electronwill.nightconfig.core.file.CommentedFileConfig;

import java.io.*;
import java.util.*;

public class Config
{
    private static Config instance;
    private final File configFile;
    private final CommentedFileConfig config;

    private final Map<String, Set<String>> serverMap;

    public Config()
    {
        super();
        configFile = new File(Unify.get().getDataDirectory().toFile(), "config.toml");
        serverMap = new HashMap<>();

        config = CommentedFileConfig.builder(configFile)
            //.defaultResource("config.toml") // this throws an error for some reasons
            .autosave()
            .build();

        load();
    }

    public static Config get()
    {
        if (instance == null)
        {
            instance = new Config();
        }

        return instance;
    }

    public CommentedFileConfig getConfig()
    {
        return config;
    }

    public void load()
    {
        Unify.get().getDataDirectory().toFile().mkdirs();

        if (!configFile.exists())
        {
            // overwrite/create default file
            try
            {
                InputStream is = Unify.class.getClassLoader().getResourceAsStream("config.toml");
                OutputStream os = new FileOutputStream(configFile);
                os.write(is.readAllBytes());
                os.close();
                is.close();
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }

        // load again
        config.load();

        // map servers
        CommentedConfig groups = config.get("groups");

        // add common servers as values
        for (CommentedConfig.Entry group : groups.entrySet())
        {
            List<String> servers = group.getValue();

            for (String s1 : servers)
            {
                if (!serverMap.containsKey(s1))
                {
                    serverMap.put(s1, new HashSet<>());
                }

                for (String s2 : servers)
                {
                    if (!s1.equalsIgnoreCase(s2))
                    {
                        serverMap.get(s1).add(s2);
                    }
                }
            }
        }
    }

    public Map<String, Set<String>> getServerMap()
    {
        return serverMap;
    }

    public String getServerMapString(String server)
    {
        return "([%s] -> %s) ".formatted(server, serverMap.get(server).toString());
    }

    public CommentedConfig getServer(String name)
    {
        List<CommentedConfig> servers = config.get("servers");
        for (CommentedConfig server : servers)
        {
            if (server.get("name").toString().equalsIgnoreCase(name))
            {
                return server;
            }
        }

        return null;
    }

    /**
     * Determine if two servers are in a group together.
     * @param name1 a server name
     * @param name2 another server name
     * @return true if both server names are present in a group, false otherwise
     */
    public boolean hasMutualGroup(String name1, String name2)
    {
        return serverMap.get(name1).contains(name2) || serverMap.get(name2).contains(name1);
    }
}
