package com.astrelion.unify;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.*;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.minimessage.tag.resolver.Placeholder;

import java.util.Optional;
import java.util.UUID;

@CommandAlias("unify|uni")
public class Commands extends BaseCommand
{
    private Unify unify;
    private Config config;
    private MiniMessage miniMessage;

    public Commands()
    {
        unify = Unify.get();
        config = Config.get();
        miniMessage = unify.getMiniMessage();
    }

    @Default
    @HelpCommand
    @Description("Show subcommands and help")
    public void onHelp(CommandIssuer sender, CommandHelp help)
    {
        help.showHelp();
    }

    @Subcommand("reload")
    //@CommandPermission("unify.reload")
    @Description("Reload the configuration from file")
    public void doReload(CommandIssuer sender)
    {
        config.load();
    }

    @Subcommand("move|tp")
    //@CommandPermission("unify.tp")
    @Description("Move the given player to another server")
    @CommandCompletion("@proxyplayers @proxyservers")
    public void doMove(CommandIssuer sender, String player, String server)
    {
        Player optionalPlayer = getOptionalPlayer(player);
        if (optionalPlayer == null)
        {
            sender.sendMessage("Invalid player.");
            return;
        }

        RegisteredServer optionalServer = getOptionalServer(server);
        if (optionalServer == null)
        {
            sender.sendMessage("Invalid server.");
            return;
        }

        optionalPlayer.sendMessage(Component.text("Sending you to " + server));
        optionalPlayer.createConnectionRequest(optionalServer).fireAndForget();
    }

    @Subcommand("msg|m|tell|t|whisper|w|r|dm")
    @Description("Send a direct message to another player on any server")
    @CommandCompletion("@proxyplayers @nothing")
    public void onMsg(CommandIssuer sender, String player, String message)
    {
        Player optionalPlayer = getOptionalPlayer(player);
        if (optionalPlayer == null)
        {
            sender.sendMessage("Invalid player.");
            return;
        }

        Player senderPlayer = getOptionalPlayer(sender.getUniqueId());
        if (senderPlayer == null)
        {
            return;
        }

        optionalPlayer.sendMessage(
            Identity.identity(sender.getUniqueId()),
            miniMessage.deserialize(config.getConfig().get("general.msgFormat"),
                Placeholder.unparsed("player", senderPlayer.getUsername()),
                Placeholder.unparsed("message", message))
        );
    }

    @Subcommand("find|f|locate")
    @Description("Find which server the given player is on")
    @CommandCompletion("@proxyplayers")
    public void onFind(CommandIssuer sender, String player)
    {
        Player optionalPlayer = getOptionalPlayer(player);
        if (optionalPlayer == null || optionalPlayer.getCurrentServer().isEmpty())
        {
            sender.sendMessage("Invalid player.");
            return;
        }

        sender.sendMessage("%s is on server %s.".formatted(player, optionalPlayer.getCurrentServer().get().getServerInfo().getName()));
    }

    @Subcommand("list|ls")
    @Description("List all players connect to the proxy, similar to /glist all")
    @CommandCompletion("@proxyservers")
    public void onList(CommandIssuer sender,  @co.aikar.commands.annotation.Optional String server)
    {
        String list;
        if (server != null)
        {
            RegisteredServer registeredServer = getOptionalServer(server);

            if (registeredServer == null)
            {
                sender.sendMessage("Invalid server.");
                return;
            }

            list = String.join(", ", registeredServer.getPlayersConnected().stream().map(Player::getUsername).toList());
        }
        else
        {
            list = String.join(", ", Unify.get().getServer().getAllPlayers().stream().map(Player::getUsername).toList());
        }

        sender.sendMessage(list);
    }

    private Player getOptionalPlayer(String player)
    {
        Optional<Player> optionalPlayer = Unify.get().getServer().getPlayer(player);

        if (optionalPlayer.isEmpty())
        {
            return null;
        }

        return optionalPlayer.get();
    }

    private Player getOptionalPlayer(UUID uuid)
    {
        Optional<Player> optionalPlayer = Unify.get().getServer().getPlayer(uuid);

        if (optionalPlayer.isEmpty())
        {
            return null;
        }

        return optionalPlayer.get();
    }

    private RegisteredServer getOptionalServer(String server)
    {
        Optional<RegisteredServer> optionalServer = Unify.get().getServer().getServer(server);

        if (optionalServer.isEmpty())
        {
            return null;
        }

        return optionalServer.get();
    }
}
