package com.astrelion.unify;

import co.aikar.commands.VelocityCommandManager;
import com.electronwill.nightconfig.core.CommentedConfig;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.velocitypowered.api.event.PostOrder;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.DisconnectEvent;
import com.velocitypowered.api.event.player.PlayerChatEvent;
import com.velocitypowered.api.event.player.ServerPostConnectEvent;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.event.proxy.ProxyPingEvent;
import com.velocitypowered.api.event.proxy.ProxyReloadEvent;
import com.velocitypowered.api.event.proxy.ProxyShutdownEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.plugin.annotation.DataDirectory;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.player.TabList;
import com.velocitypowered.api.proxy.player.TabListEntry;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import com.velocitypowered.api.proxy.server.ServerPing;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.minimessage.tag.resolver.Placeholder;
import org.slf4j.Logger;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Plugin(
    id = "unify",
    name = "Unify",
    version = BuildConstants.VERSION
)
public class Unify
{
    private static Unify instance;

    @Inject
    private ProxyServer server;
    @Inject
    private Logger logger;
    @Inject
    @DataDirectory
    private Path dataDirectory;

    private Config config;
    private MiniMessage miniMessage;
    private VelocityCommandManager commandManager;

    public Unify()
    {
        instance = this;
    }

    @Subscribe
    public void onProxyInitialization(ProxyInitializeEvent event)
    {
        config = Config.get();
        miniMessage = MiniMessage.miniMessage();

        commandManager = new VelocityCommandManager(server, this);
        commandManager.enableUnstableAPI("help");
        commandManager.getCommandCompletions().registerCompletion("proxyservers", (c) ->
            ImmutableList.copyOf(
                server.getAllServers().stream().map((x) -> x.getServerInfo().getName()).toList()
            )
        );
        commandManager.getCommandCompletions().registerCompletion("proxyplayers", (c) ->
            ImmutableList.copyOf(server.getAllPlayers().stream().map(Player::getUsername).toList())
        );
        commandManager.getCommandCompletions().registerCompletion("acceptdeny", (c) ->
            ImmutableList.of("accept", "deny")
        );
        commandManager.registerCommand(new Commands());

        logger.info("Unify initialized!");
    }

    @Subscribe
    public void onReload(ProxyReloadEvent event)
    {
        config.load();
    }

    @Subscribe
    public void onShutdown(ProxyShutdownEvent event)
    {
        config.getConfig().close();
    }

    @Subscribe(order = PostOrder.LAST)
    public void onPlayerChat(PlayerChatEvent event)
    {
        Player player = event.getPlayer();
        String message = event.getMessage();

        player.getCurrentServer().ifPresent((fromConnection) ->
        {
            RegisteredServer from = fromConnection.getServer();
            Component component = buildMessage(from, player, message);

            CommentedConfig fromConfig = config.getServer(from.getServerInfo().getName());

            if (fromConfig == null || !(boolean) fromConfig.get("send"))
            {
                return;
            }

            if (config.getConfig().get("general.logToProxy"))
            {
                TextComponent c = Component.text(config.getServerMapString(from.getServerInfo().getName()));
                c = (TextComponent) c.append(component).compact();

                logger.info(
                    c.content()
                );
            }

            for (RegisteredServer to : server.getAllServers())
            {
                CommentedConfig toConfig = config.getServer(to.getServerInfo().getName());

                if ((boolean) toConfig.get("receive") &&
                    config.hasMutualGroup(from.getServerInfo().getName(), to.getServerInfo().getName()))
                {
                    for (Player toPlayer : to.getPlayersConnected())
                    {
                        toPlayer.sendMessage(player.identity(), component);
                        //toPlayer.sendMessage(Identity.identity(player.getUniqueId()), component);
                    }
                }
            }
        });
    }

    @Subscribe
    public void onPlayerJoin(ServerPostConnectEvent event)
    {
        Player joiner = event.getPlayer();
        for (Player player : server.getAllPlayers())
        {
            System.out.println("Adding entry for " + player.getUsername());
            TabList tabList = player.getTabList();
            if (!tabList.containsEntry(joiner.getUniqueId()))
            {
                TabListEntry entry = TabListEntry.builder()
                    .tabList(tabList)
                    .profile(joiner.getGameProfile().withId(joiner.getUniqueId()))
                    .playerKey(joiner.getIdentifiedKey())
                    .build();
                tabList.addEntry(entry);
            }
        }
    }

    @Subscribe
    public void onPlayerLeave(DisconnectEvent event)
    {
        Player leaver = event.getPlayer();
        for (Player player : server.getAllPlayers())
        {
            System.out.println("Removing entry for " + player.getUsername());
            player.getTabList().removeEntry(leaver.getUniqueId());
        }
    }

    @Subscribe
    public void onProxyPing(ProxyPingEvent event)
    {
        ServerPing.Builder ping = event.getPing().asBuilder();
        String serverlist = config.getConfig().get("general.serverlistHover");
        List<ServerPing.SamplePlayer> players = new ArrayList<>();

        if (serverlist != null)
        {
            for (String line : serverlist.split("\n"))
            {
                players.add(new ServerPing.SamplePlayer(line.replaceAll("[^ -~]", ""), UUID.randomUUID()));
            }
        }
        event.setPing(ping.samplePlayers(players.toArray(new ServerPing.SamplePlayer[0])).build());
    }

    private Component buildMessage(RegisteredServer server, Player player, String message)
    {
        CommentedConfig serverConfig = config.getServer(server.getServerInfo().getName());
        if (serverConfig == null) return Component.text(message);
        String format = serverConfig.get("chatFormat");
        if (format == null) return Component.text(message);

        return miniMessage.deserialize(format,
            Placeholder.unparsed("player", player.getUsername()),
            Placeholder.unparsed("server", server.getServerInfo().getName()),
            Placeholder.unparsed("message", message));
    }

    public Path getDataDirectory()
    {
        return dataDirectory;
    }

    public Logger getLogger()
    {
        return logger;
    }

    public MiniMessage getMiniMessage()
    {
        return miniMessage;
    }

    public ProxyServer getServer()
    {
        return server;
    }

    public static Unify get()
    {
        return instance;
    }
}
