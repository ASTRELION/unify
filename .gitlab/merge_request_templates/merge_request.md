**Reason for the merge**

Describe the reason that this merge is needed.

**Content**

Describe the content of the merge. What changes did you make? What considerations went into your solution? What other options did consider before implementing your final solution?

**Testing**

[ ] Does the project successfully build and generate a new `.jar`?

[ ] Did you test your features/fixes IN GAME?

[ ] Did you test every permission or configuration change (if applicable)?

**Other Information**

Any other information you'd like to include.
