# Unify <sup>WIP</sup>

**Features**

- Unified chat between servers
- Unified tab-list
- Direct message across servers
- Move players between servers
- [Highly configurable](#configuration), with [MiniMessage](https://docs.adventure.kyori.net/minimessage/index.html) support
- [Permissable](#permissions)
- Open-source :)

## Configuration

[The default configuration can be found here.](src/main/resources/config.toml)

Format strings support [MiniMessage](https://docs.adventure.kyori.net/minimessage/index.html). Additional replacements:

- `<player>` the player name
- `<server>` the server name
- `<message>` the message content

## Commands

| Command<br/>*`<required>` `[optional]`*                            | Aliases                                     | Permission        | Description                                                                                        |
|--------------------------------------------------------------------|---------------------------------------------|-------------------|----------------------------------------------------------------------------------------------------|
| `/unify broadcast <message>`                                       | `say`                                       | `unify.broadcast` | Broadcast a message to all servers on the proxy                                                    |
| `/unify find <player>`                                             | `f`, `locate`                               | `unify.find`      | Print which server the given player is connected to                                                |
| `/unify help [command]`                                            | `h`                                         | `unify.help`      | Display help                                                                                       |
| `/unify list [server]`                                             | `ls`                                        | `unify.list`      | List all players on the proxy or on the given server                                               |
| <code>/unify move <player&#124;server> [server&#124;player]</code> | `tp`                                        | `unify.move`      | Move yourself to the given player's server, or move the given player to the given player or server |
| `/unify msg <player> <message>`                                    | `m`, `tell`, `t`, `whisper`, `w`, `r`, `dm` | `unify.msg`       | Send a direct message to the given player                                                          |
| `/unify reload`                                                    |                                             | `unify.reload`    | Reload `config.toml` from file (you can also use `/velocity reload`)                               |
| `/unify version`                                                   | `v`                                         | `unify.version`   | Display Unify version and check for updates                                                        |

*`/unify` can be replaced with `/uni` if desired.*

## Permissions

| Permission        | Description                       |
|-------------------|-----------------------------------|
| `unify.broadcast` | Allows `/unify broadcast`         |
| `unify.find`      | Allows `/unify find`              |
| `unify.help`      | Allows `/unify` and `/unify help` |
| `unify.list`      | Allows `/unify list`              |
| `unify.move`      | Allows `/unify move`              |
| `unify.msg`       | Allows `/unify msg`               | 
| `unify.reload`    | Allows `/unify reload`            |
| `unify.version`   | Allows `/unify version`           |

*I recommend using [LuckPerms](https://luckperms.net/) __on the proxy__ for permission handling.*